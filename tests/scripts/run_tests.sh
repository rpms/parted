#!/bin/sh
set -eux

# make a small disk image
fallocate -l 100M ./disk.img
parted -s ./disk.img mklabel gpt
parted -s ./disk.img 'mkpart root 1MiB 100%'
parted -m -s ./disk.img u s p
